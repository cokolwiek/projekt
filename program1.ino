
const int analogInPin = A0;    /// Wejscie analogowe na pinie A0 
const int analogOutPin = 9;    /// Wyjscie analogowe na pinie 9 
const int digitalInPin = 13;   /// Wejscie cyfrowe na pinie 13
int kier1=8;                   /// przypisanie zmiennej kier1 do pinu 8
int kier2=7;                   /// przypisanie zmiennej kier2 do pinu 7
int suma =0;
int sensorValue = 0;        // value read 
int outputValue = 0;       
int sensorValuesum=0;
int buttonState = 0;

void setup() {
  /// initialize serial communications at 9600 bps:
  Serial.begin(9600); 
  pinMode(kier1, OUTPUT);       /// ustawienie pinu 8 na wyjscie
  pinMode(kier2, OUTPUT);       /// ustawienie pinu na wyjscie
  pinMode(digitalInPin,INPUT);  /// ustawienie pinu na wejscie
}
void loop() {
 /// na podstawie 10 probek petla wylicza srednia wartosc ADC
   while(suma<10)
   {
   sensorValuesum=sensorValuesum + analogRead(analogInPin);
   suma++;
   }
   sensorValue = sensorValuesum/10;
   sensorValuesum=0;
   suma=0;
  outputValue = map(sensorValue, 701, 1023, 55, 100); /// mapowanie odczytu ADC na wyjscie PWM
  if( outputValue>0)
  {
      analogWrite(analogOutPin, outputValue);           
  }
  else
  {
     analogWrite(analogOutPin, 0); 
  }
  // wypisywanie wartosci
  Serial.print("sensor = " );                       
  Serial.print(sensorValue);  
  Serial.print("\t output = ");    
   if( outputValue>0)
  {     
  Serial.println(outputValue);  
  }
  else
  {
    Serial.println(0); 
  }
 buttonState = digitalRead(digitalInPin);  /// obsluga mostka H, sterowanie wyjsciami A1 i A2 (lewo,prawo)
  if (buttonState == HIGH) 
  {
      digitalWrite(kier1, HIGH);
      digitalWrite(kier2, LOW);
    }
      else
      {
        digitalWrite(kier1, LOW);
        digitalWrite(kier2, HIGH);
      }

  delay(2);                     
}
